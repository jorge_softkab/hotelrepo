<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categorias extends Model
{
    //
    protected $connection = 'comments';
    protected $table = 'categorias';
    public $timestamps = false;
}
